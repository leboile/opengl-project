// Used for shadow lookup
varying vec4 ShadowCoord;


varying vec3 normal, lightDirection, viewDirection;
void phong()
{	
	normal = gl_NormalMatrix * gl_Normal;//calculating normal
	vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);//world coordinates
	lightDirection = vec3(gl_LightSource[0].position.xyz - vVertex);
	viewDirection = -vVertex;//camera is always at 0 0 0
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
void main()
{
        phong();
     	ShadowCoord= gl_TextureMatrix[7] * gl_Vertex;
  
		gl_Position = ftransform();

		gl_FrontColor = gl_Color;
}