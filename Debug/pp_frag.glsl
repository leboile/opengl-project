﻿//uniform sampler2D PPtex;
//varying vec2 f_texcoord;
 
//float rand(vec2 co){
//    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
//}
//void main(void) {
 //vec4 sepia = vec4(112.0 / 255.0, 66.0 / 255.0, 20.0 / 255.0,1.0);
//  vec4 c = vec4(rand(f_texcoord));
//  gl_FragColor = texture2D(PPtex, f_texcoord)*c;
//}


uniform sampler2D PPtex;
varying vec2 f_texcoord;
uniform float rt_w; 
uniform float rt_h; 
uniform float pixel_w; 
uniform float pixel_h; 
void main() 
{ 
 float vx_offset=1.0;
 vec2 uv = f_texcoord.xy;
  
  vec3 tc = vec3(1.0, 0.0, 0.0);
  if (uv.x < (vx_offset-0.005))
  {
    float dx = pixel_w*(1./rt_w);
    float dy = pixel_h*(1./rt_h);
    vec2 coord = vec2(dx*floor(uv.x/dx),
                      dy*floor(uv.y/dy));
    tc = texture2D(PPtex, coord).rgb;
  }
  else if (uv.x>=(vx_offset+0.005))
  {
    tc = texture2D(PPtex, uv).rgb;
  }
	gl_FragColor = vec4(tc, 1.0);
}
