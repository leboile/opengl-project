#include <iostream>
#include <vector>
#include "glew\include\GL\glew.h"
#include "freeglut\include\GL\freeglut.h"
#include "glew\include\GL\wglew.h"
#include <math.h>
#include <Windows.h>
#include "ShaderManager.cpp"
#include "Shader.cpp"
using namespace std;
const double Pi = 3.1415;
const double max_fps=60.0f;
double fps=0.0f;
struct vector3d
{
	GLfloat x,y,z;
	vector3d(GLfloat x1=0.0f, GLfloat y1=0.0f, GLfloat z1=0.0f)
	{
		x=x1;
		y=y1;
		z=z1;
	}
};
class model
{
protected:
	vector3d rotation;
	GLuint id;
	vector3d position;
    CShader* shader;
public:
	 model()
	{
		rotation.x=0;
		rotation.y=0;
		rotation.z=0;
		position.x=0;
		position.y=0;
		position.z=0;
	}
	virtual GLuint getId()
	{
		return id;
	}
	virtual CShader* getShader()
	{
		return shader;
	}
	virtual void render()=0;
	virtual void animate()=0;
	virtual void setPos(vector3d xyz)=0;
	virtual vector3d getPos()=0;
	virtual void addShader(const char* vertex, const char* frag)=0;
	virtual void startTranslate()
	{
		/*glMatrixMode ( GL_MODELVIEW );
		glPushMatrix();
		glTranslatef(position.x,position.y,position.z);
		glRotated(rotation.z,0,0,1);
		glRotated(rotation.x,1,0,0);
		glRotated(rotation.y,0,1,0);*/
		glPushMatrix();
		glTranslatef(position.x,position.y,position.z);
		glRotated(rotation.z,0,0,1);
		glRotated(rotation.x,1,0,0);
		glRotated(rotation.y,0,1,0);
		glMatrixMode(GL_TEXTURE);
		glActiveTexture(GL_TEXTURE7);
		glPushMatrix();
		glTranslatef(position.x,position.y,position.z);
		glRotated(rotation.z,0,0,1);
		glRotated(rotation.x,1,0,0);
		glRotated(rotation.y,0,1,0);
	}
	virtual void endTranslate()
	{
			//glPopMatrix();

		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}

};

class torus:public model
{
	inline void TorusVertex(double r1,double r2,double phi,double psi)
	{
		double nx = cos(phi)*cos(psi);
		double ny = sin(psi);
		double nz = sin(phi)*cos(psi);
		glNormal3d(nx,ny,nz);
		glVertex3d(r1*cos(phi) + r2*nx,r2*ny,r1*sin(phi) + r2*nz);
	}
public:
	torus(double r1,double r2,int n1,int n2)
	{
		shader=NULL;
		GLuint list = glGenLists(1);
		glNewList(list,GL_COMPILE);
		glBegin(GL_QUADS);
		glColor3f(1,0,0);
		for(int i = 0;i<n1;i++) {
			int i2 = (i<n1-1)?(i+1):(0);
			double phi1 = 2*i*Pi/n1;
			double phi2 = 2*i2*Pi/n1;
			for(int j = 0;j<n2;j++) {
				int j2 = (j<n2-1)?(j+1):(0);
				double psi1 = 2*j*Pi/n2;
				double psi2 = 2*j2*Pi/n2;
				TorusVertex(r1,r2,phi1,psi1);
				TorusVertex(r1,r2,phi1,psi2);
				TorusVertex(r1,r2,phi2,psi2);
				TorusVertex(r1,r2,phi2,psi1);
			}
		}
		glEnd();
		glEndList();
		id=list;
	}
	void animate()
	{
		rotation.x+=1.5f;
		rotation.z+=0.4f;
	}
	void render()
	{
	//	glMatrixMode(GL_MODELVIEW);
		//glLoadIdentity();
		//glTranslated(position.x,position.y,position.z);
		
		startTranslate();
		glCallList(id);
		endTranslate();
	}
	void setPos(vector3d pos)
	{
		position=pos;
	}
	vector3d getPos()
	{
		return position;
	}
	void addShader(const char* vertex,const char* frag)
	{
		shader= CShaderManager::GetInstance()->GetShader(vertex,frag,NULL);
	}
};
class cube:public model
{
	GLfloat s;
public:
	cube(GLfloat size):s(size)
	{
		shader=NULL;
	}
	void animate()
	{
		rotation.x+=0.5f;
		rotation.z+=0.2f;
	}
	void render()
	{/*
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslated(position.x,position.y,position.z);
		glRotated(rotation.z,0,0,1);
		glRotated(rotation.x,1,0,0);
		glRotated(rotation.y,0,1,0);*/
		startTranslate();
	    //glCallList(id);
		glutSolidCube(s);
		endTranslate();

	}
	void setPos(vector3d pos)
	{
		position=pos;
	}
	vector3d getPos()
	{
		return position;
	}
	void addShader(const char* vertex, const char* frag)
	{
		shader= CShaderManager::GetInstance()->GetShader(vertex,frag,NULL);
	}
};

class bspline:public model
{
	vector<double> knots;
	vector<vector<vector3d> > m_verteces;
	int count;
	vector<vector<vector3d>> controlPoints;
public:
	bspline(int resolution)
	{
		//bspline coordinates
		cout<<"BSPLINE"<<endl;
		vector<vector3d> p;
		p.push_back(vector3d(10,0,0));
		p.push_back(vector3d(10,10,0));
		p.push_back(vector3d(0,10,0));
		p.push_back(vector3d(-10,10,0));
		controlPoints.push_back(p);
		p.clear();
        p.push_back(vector3d(-10,0,20));
		p.push_back(vector3d(-10,-10,20));
		p.push_back(vector3d(0,-10,0.20f));
		p.push_back(vector3d(10,-10,0.20f));
		controlPoints.push_back(p);
		p.clear();

        vector<double> knotsX = GetKnots(controlPoints[0].size());
		vector<double> knotsZ = GetKnots(controlPoints.size());
		m_verteces.clear();

		double delta = 1.0/resolution;

		for(double u=0; u<=1; u+=delta)
		{
			vector<vector3d> row;

			for(double v=0; v<=1; v+=delta)
			{
				vector3d vertex;

				for(unsigned int i=0;i<controlPoints.size();i++)
				{
					for(unsigned int j=0;j<controlPoints[i].size();j++)
					{
						float b1 = (float)BasisFunction(u,i,knotsZ,knotsZ.size()-i-1);
						float b2 = (float)BasisFunction(v,j,knotsX,knotsX.size()-j-1);
						float cx = controlPoints[i][j].x;
						float cy = controlPoints[i][j].y;
						float cz = controlPoints[i][j].z;

						vertex.x += controlPoints[i][j].x * (b1 * b2);
						vertex.y += controlPoints[i][j].y * (b1 * b2);
						vertex.z += controlPoints[i][j].z * (b1 * b2);
					}
				}
				//cout<<"END"<<endl;
				row.push_back(vertex);
			}
			m_verteces.push_back(row);
		}
		cout<<"created succesfully"<<endl;
	}
	void animate()
	{
		//rotation.x+=0.5f;
		//rotation.z+=0.2f;
	}
	void render()
	{
	/*	glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslated(position.x,position.y,position.z);
		glRotated(rotation.z,0,0,1);
		glRotated(rotation.x,1,0,0);
		glRotated(rotation.y,0,1,0);*/
		startTranslate();
		glBegin(GL_QUADS);
		{
			for(unsigned int x=0;x<m_verteces.size()-1;x++)
			{
				for(unsigned int y=0;y<m_verteces[x].size()-1;y++)
				{
					vector3d v0 = m_verteces[x][y];
					vector3d v1 = m_verteces[x][y+1];
					vector3d v2 = m_verteces[x+1][y+1];
					vector3d v3 = m_verteces[x+1][y];

					//calculating normal
					vector3d l1(v1.x-v0.x,v1.y-v0.y,v1.z-v0.z);
					vector3d l2(v3.x-v0.x,v3.y-v0.y,v3.z-v0.z);
					float nx = (l1.y*l2.z) - (l1.z*l2.y);
					float ny = (l1.z*l2.x) - (l1.x*l2.z);
					float nz = (l1.x*l2.y) - (l1.y*l2.x);
					vector3d n = vector3d(nx,ny,nz); 

					glNormal3d(n.x,n.y,n.z);//adding normal
					glVertex3d(v0.x,v0.y,v0.z);//vertices
					glVertex3d(v1.x,v1.y,v1.z);
					glVertex3d(v2.x,v2.y,v2.z);
					glVertex3d(v3.x,v3.y,v3.z);
				}
			}
		}
		glEnd();
		endTranslate();
	}
	void setPos(vector3d pos)
	{
		position=pos;
	}
	vector3d getPos()
	{
		return position;
	}
	void addShader(const char* vertex,const char* frag)
	{
		shader= CShaderManager::GetInstance()->GetShader(vertex,NULL,NULL);
	}

	vector<double> GetKnots(int numControlPoints)
	{
		vector<double> knots;
		int numKnots = numControlPoints + 4;

		for(int i=0;i<numKnots/2 && i<4;i++)
		{
			knots.push_back(0);
		}

		int innerKnots = numKnots - 8;
		if(innerKnots<0 && numKnots%2 != 0)
			innerKnots = 1;

		for(int i=1;i<=innerKnots;i++)
		{
			knots.push_back(i/(float)(innerKnots+1));
		}

		for(int i=0;i<numKnots/2 && i<4;i++)
		{
			knots.push_back(1);
		}
		return knots;
	}

	void RefreshPoints(vector<vector<vector3d> > controlPoints, int resolution)
	{
		vector<double> knotsX = GetKnots(controlPoints[0].size());
		vector<double> knotsZ = GetKnots(controlPoints.size());
		//cout<<"KNOTS FOUND"<<endl;
		m_verteces.clear();

		double delta = 1.0/resolution;

		for(double u=0; u<=1; u+=delta)
		{
			//cout<<"u: "<<u<<endl;
			vector<vector3d> row;

			for(double v=0; v<=1; v+=delta)
			{
				vector3d vertex;

				for(unsigned int i=0;i<controlPoints.size();i++)
				{
					for(unsigned int j=0;j<controlPoints[0].size();j++)
					{
						float b1 = (float)BasisFunction(u,i,knotsZ,knotsZ.size()-i+1);
						float b2 =(float) BasisFunction(v,j,knotsX,knotsX.size()-j+1);
						//cout<<"BASISFUNCTION WORKED"<<endl;
						float cx = controlPoints[i][j].x;
						float cy = controlPoints[i][j].y;
						float cz = controlPoints[i][j].z;

						vertex.x += controlPoints[i][j].x * (b1 * b2);
						vertex.y += controlPoints[i][j].y * (b1 * b2);
						vertex.z += controlPoints[i][j].z * (b1 * b2);
					}
				}

				//qDebug() << row.size() << "," << m_verteces.size() << "; " << u << "," << v <<" : " << vertex.X() << "," << vertex.Y() << "," << vertex.Z() << endl;
				//cout<<"END"<<endl;
				row.push_back(vertex);
			}

			m_verteces.push_back(row);
		}
		//cout<<"WOW"<<endl;
	}
	double BasisFunction(double u, int i, vector<double> &knots, int k)//The result is how much the current control point influences the current surface point.
	{
		double result = 0;
		//cout<<"BASIS FUNCTION"<<endl;
		if(k == 1)
		{
			if((knots[i]<knots[i+1] && knots[i] <= u && u < knots[i+1]) || (u==1 && knots[i+1]==1))
			{
				result = 1;
			}
			else
			{
				result = 0;
			}
		}
		else
		{
			double r1, r2;

			double knotDelta1 = knots[i+k-1] - knots[i];
			double knotDelta2 = knots[i+k] - knots[i+1];

			if(knotDelta1 != 0)
			{
				r1 = ((u-knots[i]) / knotDelta1) * BasisFunction(u,i,knots,k-1);
			}
			else
			{
				r1 = 0;
			}

			if(knotDelta2 != 0)
			{
				r2 = ((knots[i+k]-u) / knotDelta2) * BasisFunction(u,i+1,knots,k-1);
			}
			else
			{
				r2 = 0;
			}

			result = r1 + r2;
		}

		return result;
	}
};

struct ModelsToRender//maybe change the names of the variables
{
	vector<model*> toRender;
	vector<torus*> toruses;
	vector<cube*> cubes;
	vector<bspline* > bsplines;
	CShader** shader;
	CShader* smShader;
	GLuint shadowMapUniform;
	GLuint shadowMapStepXUniform;
	GLuint shadowMapStepYUniform;
	int lightMov;
	int smSize;
	int width;
	int height;
	vector3d camPos,lightPos,centerPos,mirrorPos;
    GLuint FBO;
	GLuint depthTexture;

	bool ppEnabled;
	GLuint PPFBO;
	GLuint PPtex;
	GLuint PPrender;
	GLuint vbo_fbo_vertices;
	GLuint postProcAttr;
	GLuint postProcUniform;
	GLuint postProcH;
	GLuint postProcW;
	GLuint postProcPWUniform;
	GLuint postProcPHUniform;
	GLfloat postProcPixelW;
	GLfloat postProcPixelH;
	CShader* ppshader;
public:
	ModelsToRender(int w, int h,int shadowMapSize,vector3d cP, vector3d lP, vector3d centerP)
	{
		lightMov=1;
	    postProcPixelW=10.0;
		postProcPixelH=10.0;
		ppEnabled=false;
		camPos=cP;
		lightPos=lP;
		centerPos=centerP;
		mirrorPos=vector3d(-5,0,-15);
		width=w;
		height=h;
		smSize=shadowMapSize;
	    initPPFBO();//post-process FBO
		initFBO();//shadow FBO
	
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);  //for mirror  
		glClearColor(0,0,0,1.0f);
		glEnable(GL_CULL_FACE);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	}
	void initPPFBO()
	{
		//texture
		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &PPtex);
		glBindTexture(GL_TEXTURE_2D, PPtex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width,height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		


		//render buffer 
		glGenRenderbuffers(1, &PPrender);
		glBindRenderbuffer(GL_RENDERBUFFER, PPrender);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);


		/* Framebuffer to link everything together */
		glGenFramebuffers(1, &PPFBO);
		glBindFramebuffer(GL_FRAMEBUFFER,PPFBO);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, PPtex, 0);
	    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, PPrender);

		// error checking
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			cout<<"SOMETHING WENT WRONG"<<endl;
			throw "ERROR";
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		ppshader=CShaderManager::GetInstance()->GetShader("pp_vertex.glsl","pp_frag.glsl",NULL);
		glBindAttribLocation(ppshader->GetProgram(),0, "v_coord");
		postProcAttr = glGetAttribLocation(ppshader->GetProgram(),"v_coord");
		postProcUniform = glGetUniformLocation(ppshader->GetProgram(), "PPtex");
		postProcH=glGetUniformLocation(ppshader->GetProgram(), "rt_h");
		postProcW=glGetUniformLocation(ppshader->GetProgram(), "rt_w");
		postProcPWUniform=glGetUniformLocation(ppshader->GetProgram(), "pixel_w");
	    postProcPHUniform=glGetUniformLocation(ppshader->GetProgram(), "pixel_h");
		cout<<postProcAttr<<" "<<postProcUniform<<endl;
		GLfloat fbo_vertices[] = {
			-1, -1,
			1, -1,
			-1,  1,
			1,  1,
		};
		glGenBuffers(1, &vbo_fbo_vertices);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
		glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_vertices), fbo_vertices, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	}
	void initFBO()
	{
		smShader =  CShaderManager::GetInstance()->GetShader("sm_phong_vertex.glsl","sm_phong_frag.glsl",NULL);
		shadowMapUniform = glGetUniformLocation(smShader->GetProgram(),"ShadowMap");
		shadowMapStepXUniform = glGetUniformLocation(smShader->GetProgram(),"xPixelOffset");
	    shadowMapStepYUniform = glGetUniformLocation(smShader->GetProgram(),"yPixelOffset");
		//FBO
		FBO=0;
        glGenFramebuffers(1, &FBO);
        glBindFramebuffer(GL_FRAMEBUFFER, FBO);

		// Depth texture. Slower than a depth buffer, but you can sample it later in your shader
		glGenTextures(1, &depthTexture);
		glBindTexture(GL_TEXTURE_2D, depthTexture);
		// No need to force GL_DEPTH_COMPONENT24, drivers usually give you the max precision if available 
		glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT, smSize, smSize, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);

		//Poor filtering, but necessary
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		// Remove artefact on the edges of the shadowmap
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		// This is to allow usage of shadow2DProj function in the shader
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY); 
		//attach texture
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		// error checking
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			cout<<"SOMETHING WENT WRONG"<<endl;
			throw "ERROR";
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	void setTextureMatrix(void)
	{
		static double modelView[16];
		static double projection[16];

		// This is matrix transform every coordinate x,y,z
		// x = x* 0.5 + 0.5 
		// y = y* 0.5 + 0.5 
		// z = z* 0.5 + 0.5 
		// Moving from unit cube [-1,1] to [0,1]  
		const GLdouble bias[16] = {	
			0.5, 0.0, 0.0, 0.0, 
			0.0, 0.5, 0.0, 0.0,
			0.0, 0.0, 0.5, 0.0,
			0.5, 0.5, 0.5, 1.0};

			// Grab modelview and transformation matrices
			glGetDoublev(GL_MODELVIEW_MATRIX, modelView);
			glGetDoublev(GL_PROJECTION_MATRIX, projection);


			glMatrixMode(GL_TEXTURE);
			glActiveTexture(GL_TEXTURE7);

			glLoadIdentity();	
			glLoadMatrixd(bias);

			// concatating all matrice into one.
			glMultMatrixd (projection);
			glMultMatrixd (modelView);

			// Go back to normal matrix mode
			glMatrixMode(GL_MODELVIEW);
	}
	void setupMatrices(float position_x,float position_y,float position_z,float lookAt_x,float lookAt_y,float lookAt_z)
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(45,width/height,10,40000);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		gluLookAt(position_x,position_y,position_z,lookAt_x,lookAt_y,lookAt_z,0,1,0);
	}
	void addTorus(double r1,double r2,int n1,int n2)
	{
		torus* t=new torus(r1,r2,n1,n2);
		toruses.push_back(t);
		toRender.push_back(t);
	}
	void addCube(float size)
	{
		cube* c = new cube(size);
		cubes.push_back(c);
		toRender.push_back(c);
	}
	void addSplineModel(int res)
	{
		bspline* b=new bspline(res);
		bsplines.push_back(b);
		toRender.push_back(b);
	}

	void renderToSM()
	{
		// Render to our framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER,FBO);
		glViewport(0,0,smSize,smSize); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		//Using the fixed pipeline to render to the depthbuffer
		glUseProgram(0);

		// Clear previous frame values
		glClear( GL_DEPTH_BUFFER_BIT);

		//Disable color rendering, we only want to write to the Z-Buffer
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); 

		setupMatrices(lightPos.x,lightPos.y,lightPos.z,centerPos.x,centerPos.y,centerPos.z);

		// Culling switching, rendering only backface, this is done to avoid self-shadowing
		glCullFace(GL_FRONT);
		drawObjects();
		//drawMirror();

		//Save modelview/projection matrice into texture7, also add a biais
		setTextureMatrix();

		glBindFramebuffer(GL_FRAMEBUFFER,0);//setting framebuffer back
	}

	void render()
	{
		/*setupMatrices(camPos.x,camPos.y,camPos.z,centerPos.x,centerPos.y,centerPos.z);
		drawMirror();
		return;*/
		renderToSM();
		if (ppEnabled)
		{
			glBindFramebuffer(GL_FRAMEBUFFER,PPFBO);
			glUseProgram(0);
			setupMatrices(camPos.x,camPos.y,camPos.z,centerPos.x,centerPos.y,centerPos.z);
			glViewport(0, 0, width, height);
		}
		renderWithShadows();
		//drawObjects();
		drawMirror();
		if (ppEnabled)
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0); // unbind
			glClearColor(0.0, 0.0, 0.0, 1.0);
			glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE); 
			setupMatrices(camPos.x,camPos.y,camPos.z,centerPos.x,centerPos.y,centerPos.z);
			glUseProgram(ppshader->GetProgram());
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, PPtex);
			glUniform1i(postProcUniform, /*GL_TEXTURE*/0);
			glUniform1f(postProcW,(float)width);
			glUniform1f(postProcH,(float)height);
			glUniform1f(postProcPWUniform,postProcPixelW);
			glUniform1f(postProcPHUniform,postProcPixelH);
			glEnableVertexAttribArray(postProcAttr);
			glBindBuffer(GL_ARRAY_BUFFER, vbo_fbo_vertices);
			glVertexAttribPointer(
				postProcAttr,  // attribute
				2,                  // number of elements per vertex, here (x,y)
				GL_FLOAT,           // the type of each element
				GL_FALSE,           // take our values as-is
				0,                  // no extra data between each position
				0                   // offset of first element
				);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			glDisableVertexAttribArray(postProcAttr);
		}
	}

	void reflectionmatrix(GLfloat reflection_matrix[4][4],GLfloat plane_point[3],GLfloat plane_normal[3])
	{
		GLfloat* p = (GLfloat*)plane_point;
		GLfloat* v = (GLfloat*)plane_normal;
		float pv = p[0]*v[0]+p[1]*v[1]+p[2]*v[2];
		reflection_matrix[0][0] = 1 - 2 * v[0] * v[0];
		reflection_matrix[1][0] = - 2 * v[0] * v[1];
		reflection_matrix[2][0] = - 2 * v[0] * v[2];
		reflection_matrix[3][0] = 2 * pv * v[0];
		reflection_matrix[0][1] = - 2 * v[0] * v[1];
		reflection_matrix[1][1] = 1- 2 * v[1] * v[1];
		reflection_matrix[2][1] = - 2 * v[1] * v[2];
		reflection_matrix[3][1] = 2 * pv * v[1];
		reflection_matrix[0][2] = - 2 * v[0] * v[2];
		reflection_matrix[1][2] = - 2 * v[1] * v[2];
		reflection_matrix[2][2] = 1 - 2 * v[2] * v[2];
		reflection_matrix[3][2] = 2 * pv * v[2];
		reflection_matrix[0][3] = 0;
		reflection_matrix[1][3] = 0;
		reflection_matrix[2][3] = 0;
		reflection_matrix[3][3] = 1;
	}
	void renderWithShadows()
	{
		// Now rendering from the camera POV, using the FBO to generate shadows
		glViewport(0,0,width,height);
		//Enabling color write (previously disabled for light POV z-buffer rendering)
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE); 

		// Clear previous frame values
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Using the shadow shader
		glUseProgram(smShader->GetProgram());
		glUniform1i(shadowMapUniform,7);
		glUniform1f(shadowMapStepXUniform,1.0f/ (smSize));
		glUniform1f(shadowMapStepYUniform,1.0f/ (smSize));
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D,depthTexture);

		setupMatrices(camPos.x,camPos.y,camPos.z,centerPos.x,centerPos.y,centerPos.z);

		glCullFace(GL_BACK);
		drawObjects();
		glUseProgram(0);
	}
	void drawMirror()
	{
		float reflection_matrix[4][4];
		float point[3]={-18,5,0};
		float normal[3]={1,0,0};
		//glClearStencil(0);
		//drawObjects();
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); //disable the color mask

		glEnable(GL_STENCIL_TEST);
		/* ������� ������ ��������� � �������� � ������ �����
		����� 1*/
		glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
		glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	
	/*	glColor4f(1,1,1,1);
		glBegin(GL_QUADS);
		glNormal3d(0,0,1);
		glVertex3f(-18,0,-18);
		glVertex3f(18,0,-18);
		glVertex3f(18,20,-18);
		glVertex3f(-18,20,-18);
		glEnd();*/
		mirror();

		glDepthRange(1,1); // always
		glDepthFunc(GL_ALWAYS); // write the farthest depth value
		glStencilFunc(GL_EQUAL, 1, ~0); // match mirror�s visible pixels
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP); // do not change stencil values

		//Drawing mirror
	/*	glColor4f(1,1,1,1);
		glBegin(GL_QUADS);
		glNormal3d(0,0,1);
		glVertex3f(-18,0,-18);
		glVertex3f(18,0,-18);
		glVertex3f(18,18,-18);
		glVertex3f(-18,18,-18);
		glEnd();*/
		mirror();


		glDepthFunc(GL_LESS);
		glColorMask(1,1,1,1);
		glDepthRange(0,1);

		/* ������� ��������� � ���� ���� ������ ������ ����
		�������� � ������ ����� ����� 1 */
		glStencilFunc (GL_EQUAL, 0x1, 0xffffffff);
		/* ������ �� ������ � ������ */
		glStencilOp (GL_KEEP, GL_KEEP, GL_KEEP);
		//glDisable(GL_DEPTH_TEST);

		glPushMatrix();
		reflectionmatrix(reflection_matrix,point,normal);
		glMultMatrixf((float*)reflection_matrix);
		glCullFace(GL_FRONT);
		drawObjects();
		//renderWithShadows();
		glCullFace(GL_BACK);
		glPopMatrix();
		glDisable(GL_STENCIL_TEST);
		glColorMask(0,0,0,0);
		glStencilOp(GL_KEEP, GL_KEEP, GL_ZERO);
		glDepthFunc(GL_ALWAYS);

		//Drawing mirror
		/*glColor4f(1,1,1,1);
		glBegin(GL_QUADS);
		glNormal3d(0,0,1);
		glVertex3f(-18,0,-18);
		glVertex3f(18,0,-18);
		glVertex3f(18,18,-18);
		glVertex3f(-18,18,-18);
		glEnd();*/

		mirror();
		glDepthFunc(GL_LESS);
		glColorMask(1,1,1,1);

		
	}
	void mirror()
	{
		glColor3d(1,0,0);
		glBegin(GL_QUADS);
		glNormal3f(1,0,0);
		glVertex3f(-19,0,-12);
		glVertex3f(-19,18, -12);
		glVertex3f(-19,18, 50);
		glVertex3f(-19,0,50);
		glEnd();
	}
	void drawObjects()
	{
		vector<model*>::iterator it;
		for(it=toRender.begin(); it!=toRender.end(); it++)
		{
			glColor3d(1,1,1);
			(*it)->render();
		}


		glColor4f(0,0,1,1);
		glBegin(GL_QUADS);
		//glNormal3d(0,0,-1);
		glVertex3f(-20,-2,-20);
		glVertex3f(20,-2,-20);
		glVertex3f(20,20,-20);
		glVertex3f(-20,20,-20);
		glEnd();

		glColor3d(1,1,1);
		glBegin(GL_QUADS);
		glNormal3f(0,1,0);
		glVertex3f(-100,-2,-20);
		glVertex3f(-100,-2, 50);
		glVertex3f( 100,-2, 50);
		glVertex3f( 100,-2,-20);
		glEnd();
		
		glColor3d(1,0,0);
		glBegin(GL_QUADS);
		glNormal3f(1,0,0);
		glVertex3f(-20,-2,-20);
		glVertex3f(-20,20, -20);
		glVertex3f(-20,20, 50);
		glVertex3f( -20,-2,50);
		glEnd();

		glColor3d(0,1,0);
		glBegin(GL_QUADS);
		glNormal3f(-1,0,0);
		glVertex3f(20,-2,-20);
		glVertex3f(20,-2,50);
		glVertex3f(20,20, 50);
		glVertex3f(20,20, -20);
		glEnd();

		//glColor3d(0,1,0);//roof
		//glBegin(GL_QUADS);
		//glNormal3f(0,-1,0);
		//glVertex3f(20,50, 50);
		//glVertex3f(-20, 50, 50);
		//glVertex3f(-20,50,-20);
		//glVertex3f(20,50,-20);
		//glEnd();


	//	glColor4f(1,1,0,1);
	//	glNormal3d(0,0,1);
	//	glBegin(GL_QUADS);
	//	
	//    glVertex3f(-20,-2,50);
	//	glVertex3f(20,-2,50);
	//	glVertex3f(20,20,50);
	//	glVertex3f(-20,20,50);
	//glEnd();
	}
	void animate()
	{
		if (lightPos.y>60)
			lightMov=-1;
		if (lightPos.y<10)
			lightMov=1;

		lightPos.y+=lightMov*0.1f;
		vector<model*>::iterator it;
		for(it=toRender.begin(); it!=toRender.end(); it++)
		{
			(*it)->animate();
		}
	}
	void addShader(const char* vertex,const char* frag)
	{
		//shader= CShaderManager::GetInstance()->GetShader(vertex,frag,NULL);
	}
}* renderList;

void getFps()
{
	static int framesCount=0;
	static int previousTime=0;
	framesCount++;
	int currentTime=glutGet(GLUT_ELAPSED_TIME);
	int timeInterval = currentTime - previousTime;
	if (timeInterval>1000)
	{
		fps = framesCount / (timeInterval / 1000.0f);
		cout<<fps<<endl;
		previousTime=currentTime;
		framesCount=0;
	}
}
void reshape(int w, int h)
{ /* ����� ������������ ��������� �������� ���� */
	if(h > 0) {
		glViewport(0,0,w,h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(45.0,w/(GLdouble)h,1,20);
	}
}
void display(void)
{ /* ����� ���������� ������� ��������� */ 
	glClearColor(0.0,0.0,0.0,0.0);
	glClearStencil(0);
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
glEnable(GL_DEPTH_BUFFER_BIT);
glDisable(GL_STENCIL_TEST);
	renderList->render();
	glFlush();
	glutSwapBuffers();
}
void idle(void)
{ /* ����� ���������� �������� */ 
	//glUseProgram(renderList->ppshader->GetProgram());
	//GLfloat move = glutGet(GLUT_ELAPSED_TIME) / 1000.0 * 2*3.14159 * .75;  // 3/4 of a wave cycle per second
	//glUniform1f(uniform_offset, move);
	renderList->animate();
	getFps();
	display();
}
void initialize () 
{
	glClearColor   ( 0.0, 0.0, 0.0, 1.0 );
    glEnable       ( GL_DEPTH_TEST );
	typedef bool (APIENTRY *PFNWGLSWAPINTERVALFARPROC)(int);
	PFNWGLSWAPINTERVALFARPROC wglSwapIntervalEXT = 0;
	wglSwapIntervalEXT = (PFNWGLSWAPINTERVALFARPROC)wglGetProcAddress("wglSwapIntervalEXT");
	wglSwapIntervalEXT(1);//VSYNC
	renderList=new ModelsToRender(1280,1024,2048,vector3d(0,10,60),vector3d(0,20,60),vector3d(0,0,-10));
	//renderList->addShader("phong_light_vertex.glsl","phong_light_frag.glsl");
	const double r1 = 2.0;
	const double r2 = 0.25;
	const int n1 = 100;
	const int n2 = 70; 
	renderList->addTorus(r1/2,r2,n1,n2);
	renderList->toruses.back()->setPos(vector3d(-10,2,0));
	renderList->addTorus(r1/2,r2,n1,n2);
	renderList->toruses.back()->setPos(vector3d(-10,2,10));
	renderList->addTorus(2,0.25,n1,n2);
	renderList->toruses.back()->setPos(vector3d(10,6,10));
	renderList->addTorus(2,0.25,n1,n2);
	renderList->toruses.back()->setPos(vector3d(10,6,20));

	renderList->addCube(2);
	renderList->cubes.back()->setPos(vector3d(-10,5,-10));

	renderList->addCube(3);
	renderList->cubes.back()->setPos(vector3d(-10,10,-10));

	renderList->addCube(1);
    renderList->cubes.back()->setPos(vector3d(-10,2,-10));

	renderList->addCube(1);
	renderList->cubes.back()->setPos(vector3d(10,6,20));
	renderList->addCube(1);
	renderList->cubes.back()->setPos(vector3d(10,6,10));
	renderList->addSplineModel(30);
	renderList->bsplines.back()->setPos(vector3d(0,10,20));
}

void keyHits(unsigned char key, int x, int y) 
{
	switch(key)
	{
	case 27:
		exit(0);
		break;
	case 's':
		//renderList->camPos.z+=0.3;
		renderList->postProcPixelH-=0.3f;
		renderList->postProcPixelW-=0.3f;
		break;
	case 'w':
		//renderList->camPos.z-=0.3;
		renderList->postProcPixelH+=0.3f;
		renderList->postProcPixelW+=0.3f;
		break;
	case ' ':
		renderList->ppEnabled=!renderList->ppEnabled;
		break;

  	};

}
int main(int argc,char* argv[])
{   
	glutInit(&argc, argv);
    glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH| GLUT_STENCIL);
	glutInitWindowSize(1280,1024);
	glutCreateWindow("Scene");	
	GLenum err=glewInit();
    if(err!=GLEW_OK)
    {
		cout<<"GLEW FAILED"<<endl;
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    } 
	else
	{
		cout<<"GLEW LOADED"<<endl;
		fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
	}

	if (GLEW_VERSION_2_0)
	{
		cout<<"OK"<<endl;
	}
	else
		cout<<"WHAAAA???"<<endl;
	initialize();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(idle);
	glutKeyboardFunc(keyHits);
	glutMainLoop();

	return 0;
}